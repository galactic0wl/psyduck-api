const expect = require("chai").expect;
const App = require("../source/app");

describe("App.hello(user)", function() {
    it("Should return correct hello world string for given user", function () {
        // Arrange
        const app = new App();
        var user = "TestUser";
        var expected = "Hello World from: TestUser";

        // Act
        var actual = app.hello(user);

        // Assert
        expect(expected).to.be.equal(actual);
    });
});