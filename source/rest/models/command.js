module.exports = (sequelize, DataTypes) => {
    return sequelize.define('command', {
        addedBy: DataTypes.STRING
    });
}