module.exports = (sequelize, DataTypes) => {
    return sequelize.define('quote', {
        content: DataTypes.TEXT,
        capturerId: DataTypes.STRING
    });
}