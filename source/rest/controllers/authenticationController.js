// authentication controller routes
var express = require('express');
var router = express.Router();

// get /api/authentication/
router.get('/',(req,res) => {
  res.send('GET response');
});

// post /api/authentication/
router.post('/',(req,res) => {
  res.send('POST response');
});

// put /api/authentication/
router.put('/',(req,res) => {
  res.send('PUT response');
});

// delete /api/authentication/
router.delete('/',(req,res) => {
  res.send('DELETE response');
});

module.exports = router;