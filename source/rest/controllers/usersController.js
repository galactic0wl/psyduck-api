// users controller routes
var express = require('express');
var router = express.Router();

const { User, UserQuote, UserAttributes, Rank, Bank } = require('../../db');

// get /api/users/
router.get('/', async (req, res) => {
  const users = await User.findAll({
    include: [
      { model: UserAttributes },
      { model: UserQuote, as: 'quotes' },
      { model: Bank, as: 'bank' },
      { model: Rank, as: 'ranks', through: 'userRanks' }
    ]
  });
  return res.send(users);
});

// post /api/users/
router.post('/', (req, res) => {
  res.send('POST response');
});

// put /api/users/
router.put('/', (req, res) => {
  res.send('PUT response');
});

// delete /api/users/
router.delete('/', (req, res) => {
  res.send('DELETE response');
});

module.exports = router;