require('dotenv').config()

var express     = require('express');
var bodyParser  = require('body-parser');

var app         = express(); // Please do not remove this line, since CLI uses this line as guidance to import new controllers


var usersController = require('./controllers/usersController');
app.use('/api/users', usersController);


var authenticationController = require('./controllers/authenticationController');
app.use('/api/authentication', authenticationController);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(process.env.PORT || 3000, () => {
  console.log('Server is running');
});